/* Hexxagon [for the web]
 * 
 * Original game by  Argo Games
 *    This clone by  Art McBain
 *
 * Hexxagon 1 and 2 are considered abandonware:
 *    ttp://www.abandonia.com/en/games/348/Hexxagon.html
 * 
 * 
 * Icons provided by http://p.yusukekamiyamane.com/ from the Fugue set.
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 * 
 * Cursors provided by Marz {Matt} ( http://www.rw-designer.com/cursor-set/blue-insanity-d )
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 *
 * Source code:
 *   https://bitbucket.org/AMcBain/hexxagon
 */

/**
 * Map for player token names to images, relative to the location of the main HTML page.
 */
var TOKENS = {
   "Magic": "images/wand.png",
   "Water": "images/water.png",
  "Magnet": "images/magnet.png",
    "Coin": "images/point.png"
};

/**
 * Colors representative of the player tokens, used for the bars in the scoring area.
 */
var COLORS = {
   "Magic": "#e0e0e0",
   "Water": "#80a0c0",
  "Magnet": "#e06060",
    "Coin": "#f0c060",
   // Colors for various interface elements.
           "location": "#ccc",
  "location-selected": "#eeeeec",
  "location-adjacent":"#4e9a06",
      "location-jump": "#fce94f"
};

/**
 * A map for even/odd columns to types of locations, adjacent and jump, containing
 * arrays of offsets relative to a selected location to higlight.
 */
var LOCATION_SELECT = {
  even: {
    adjacent: [
      {column: -1, row: -1},
      {column: -1, row: 0},
      {column: 0, row: -1},
      {column: 0, row: 1},
      {column: 1, row: -1},
      {column: 1, row: 0}
    ],
    jump: [
      {column: -2, row: -1},
      {column: -2, row: 0},
      {column: -2, row: 1},
      {column: -1, row: -2},
      {column: -1, row: 1},
      {column: 0, row: -2},
      {column: 0, row: 2},
      {column: 1, row: -2},
      {column: 1, row: 1},
      {column: 2, row: -1},
      {column: 2, row: 0},
      {column: 2, row: 1},
    ]
  },
  odd: {
    adjacent: [
      {column: -1, row: 0},
      {column: -1, row: 1},
      {column: 0, row: -1},
      {column: 0, row: 1},
      {column: 1, row: 0},
      {column: 1, row: 1}
    ],
    jump: [
      {column: -2, row: -1},
      {column: -2, row: 0},
      {column: -2, row: 1},
      {column: -1, row: -1},
      {column: -1, row: 2},
      {column: 0, row: -2},
      {column: 0, row: 2},
      {column: 1, row: -1},
      {column: 1, row: 2},
      {column: 2, row: -1},
      {column: 2, row: 0},
      {column: 2, row: 1},
    ]
  }
};
