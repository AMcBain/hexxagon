/* Hexxagon [for the web]
 * 
 * Original game by  Argo Games
 *    This clone by  Art McBain
 *
 * Hexxagon 1 and 2 are considered abandonware:
 *   http://www.abandonia.com/en/games/348/Hexxagon.html
 * 
 * 
 * Icons provided by http://p.yusukekamiyamane.com/ from the Fugue set.
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 * 
 * Cursors provided by Marz {Matt} ( http://www.rw-designer.com/cursor-set/blue-insanity-d )
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 *
 * Source code:
 *   https://bitbucket.org/AMcBain/hexxagon
 */

/**
 * Map of board layouts by name to the ASCII-based syntax used to create them.
 * <br/><br/>
 * Each row in the layout arrays is a column, with the last row being the first column.
 * The first column is considered odd, with odd columns being offset by 0 and even
 * columns by 1/2. The left of a row is the top and the right is the bottom.
 * <br/><br/>
 * Spaces denote empty areas and anything else is considered a valid location. Trailing
 * spaces in a column are ignored. Numbered locations indicate where a player's initial
 * tokens are placed. Valid player numbers are 1 and 2.
 */
var LAYOUTS = {

  /**
   * The original Hexxagon default board layout.
   */
  original: [
    "  1xxx2",
    " xxxxxx",
    " xxxxxxx",
    "xxxx xxx",
    "2xx xxxx1",
    "xxxx xxx",
    " xxxxxxx",
    " xxxxxx",
    "  1xxx2"
  ],

  /**
   * A larger board used for to test the board creation code.
   */
  large: [
    " 1x x2",
    "xxxxxx",
    " x x x",
    "xxxxxx",
    "2x x x1",
    "xxxxxx",
    " x x x",
    "xxxxxx",
    " 1x x2"
  ]
};
