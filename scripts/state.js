/* Hexxagon [for the web]
 * 
 * Original game by  Argo Games
 *    This clone by  Art McBain
 *
 * Hexxagon 1 and 2 are considered abandonware:
 *   http://www.abandonia.com/en/games/348/Hexxagon.html
 * 
 * 
 * Icons provided by http://p.yusukekamiyamane.com/ from the Fugue set.
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 * 
 * Cursors provided by Marz {Matt} ( http://www.rw-designer.com/cursor-set/blue-insanity-d )
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 *
 * Source code:
 *   https://bitbucket.org/AMcBain/hexxagon
 */

/**
 * @namespace Manages saved state for later retrieval, perhaps in a different browser session.
 */
var State = {

  /**
   * Returns any saved state, or null if none.
   * 
   * @returns an Object
   */
  get: function() {},

  /**
   * Saves the given object.
   * 
   * @param {Object} object to save
   */
  set: function(object) {},

  /**
   * Clears any saved state.
   */
  clear: function() {},

  /**
   * Whether or not any saved state exists.
   * 
   * @returns a boolean
   */
  exists: function() {}
};

if(Modernizr.localstorage && "JSON" in this) {

  State.get = function() {
    return (localStorage.getItem("hexxagon.data")? JSON.parse(localStorage.getItem("hexxagon.data")) : null);
  };

  State.set = function(object) {
    localStorage.setItem("hexxagon.data", JSON.stringify(object));
  };

  State.clear = function() {
    localStorage.removeItem("hexxagon.data");
  };

  State.exists = function() {
    return (localStorage.getItem("hexxagon.data") != null);
  };
}
