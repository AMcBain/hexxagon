/* Hexxagon [for the web]
 * 
 * Original game by  Argo Games
 *    This clone by  Art McBain
 *
 * Hexxagon 1 and 2 are considered abandonware:
 *   http://www.abandonia.com/en/games/348/Hexxagon.html
 * 
 * 
 * Icons provided by http://p.yusukekamiyamane.com/ from the Fugue set.
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 * 
 * Cursors provided by Marz {Matt} ( http://www.rw-designer.com/cursor-set/blue-insanity-d )
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 */

/**
 * @class Manages the view portion of the game, the board.
 * @constructor
 * @param {Object} model representing the current state of the game
 * @param {Function} click function to be called when a position on the board is clicked
 */
function Board(model, click) {

  // Remove old event handlers.
  $(window).unbind("resize.board");

  // Remove any old CSS rules.
  var rule;
  for(rule in Board.rules.dynamic) {
    removeRule(Board.rules[rule], STYLES["dynamic"]);
  }
  for(rule in Board.rules.animation) {
    removeRule(Board.rules[rule], STYLES["animation"]);
  }

  // Scoring area
  var scores = Board.setupScoring(model);

  // 
  var board = Board.createBoard(model.data, model.players, click);
  var largestColumn = board[1];
  board = board[0];

  // Handle dynamic board sizing.
  $(window).resize(function() {
    Board.resize(board.length, largestColumn);
  });
  $(window).resize();

  var currentPlayer;

  /**
   * Sets or removes a className on the location at the given column and row.
   * 
   * @private
   * @param {Number} column of the location
   * @param {Number} row of the location
   * @param {boolean} add whether the className should be added
   * @param {string} className to add or remove
   */
  this.state = function(column, row, add, className) {
    var location = safeIndex(board, column, row);
    if(location) {
      if(add) {
        location.addClass(className);
      } else {
        location.removeClass(className);
      }
    }
  };

  /**
   * Sets the selected state of the location at the given column and row.
   * 
   * @param {Number} column of the location
   * @param {Number} row of the location
   * @param {boolean} selected whether to select or unselect the location
   */
  this.select = function(column, row, selected) {
    this.state(column, row, selected, "selected");
  };

  /**
   * Sets the current player (1 or 2).
   */
  this.setPlayer = function(player) {
    var board = $("#board");

    if(currentPlayer) {
      board.removeClass("player" + currentPlayer);
    }
    board.addClass("player" + (currentPlayer = player));
  };

  /**
   * Sets the adjacent state of the location at the given column and row.
   * 
   * @param {Number} column of the location
   * @param {Number} row of the location
   * @param {boolean} adjacent whether the location is adjacent or not
   */
  this.adjacent = function(column, row, adjacent) {
    this.state(column, row, adjacent, "adjacent");
  };

  /**
   * Sets the jump state of the location at the given column and row.
   * 
   * @param {Number} column of the location
   * @param {Number} row of the location
   * @param {boolean} jump whether the location is jump or not
   */
  this.jump = function(column, row, jump) {
    this.state(column, row, jump, "jump");
  };

  /**
   * Returns whether the given location has a positive adjacent state as set by {@link #adjacent}.
   * 
   * @param {Number} column of the location
   * @param {Number} row of the location
   * @returns {boolean} whether the given location is adjacent
   */
  this.isAdjacent = function(column, row) {
    var location = safeIndex(board, column, row);
    if(location) {
      return location.hasClass("adjacent");
    }
    return false;
  };

  /**
   * Move the given player to the indicated location, adding a new token there. If both
   * destination and source locations are provided, the token is moved from the source location
   * to the destination. Any tokens existing at the destination location are removed before the
   * new or moved token is placed there.
   * 
   * @param {Number} player number (1 or 2)
   * @param {Number} destColumn column of the destination location
   * @param {Number} destRow row of the destination location
   * @param {Number} [srcColumn] column of the source location
   * @param {Number} [srcRow] row of the source location
   */
  this.move = function(player, destColumn, destRow, srcColumn, srcRow) {

    var destLocation = safeIndex(board, destColumn, destRow);
    var srcLocation = safeIndex(board, srcColumn, srcRow);

    if(destLocation) {
      destLocation.children(":first").remove();

      if(!srcLocation) {
        destLocation.append($("<img src='" + TOKENS[model.players[player]] + "'/>"));
      } else {
        var img = srcLocation.children(":first").detach();
        destLocation.append(img);
      }
    }
  };

  /**
   * Sets the score for the a player to the given value.
   * 
   * @param {Number} player number (1 or 2)
   * @param {Number} value the player's score
   */
  this.score = function(player, value) {
    scores[player].html("<span>" + value + "</span>");
    scores[player].css("width", value * 5);
  };
};


/**
 * Sets the visible status of the board.
 * 
 * @param {boolean} visible whether the board should be visible
 */
Board.prototype.setVisible = function(visible) {
  var boardElement = $("#board");

  if(visible) {
    // Animates away the holes in the board.
    var transform = function() {
      if(Modernizr.csstransforms) {

        var angle = 0;
        var transformInterval = setInterval(function() {

          // The more the object rotates, the smaller it should get.
          var rule = className("transform") + ": ";
          rule += "scaleX(" + (1 - (angle / 360)) + ") scaleY(" + (1 - (angle / 360)) + ")";

          // If we include the rotateY and the browser doesn't support it,
          // the entire rule is thrown out.
          if(Modernizr.csstransforms3d) {
            rule += " rotateY(" + angle + "deg)";
          }
          insertRule("#board .hole", rule, STYLES["animation"]);

          if((angle += 4) >= 360) {
            clearInterval(transformInterval);
            removeRule("#board .hole", STYLES["animation"]);
            boardElement.addClass("ready");
            boardElement.unbind(eventName("transition") + ".board");
          }
        }, 1);
      } else {
        boardElement.addClass("ready");
      }
    };

    // Feature detection for fade-in and animation of holes.
    if(Modernizr.csstransitions) {
      boardElement.bind(eventName("transition") + ".board", transform);
      boardElement.addClass("active");
    } else {
      boardElement.addClass("active");
      transform();
    }
  } else {
    boardElement.removeClass("disabled");
    boardElement.removeClass("active");

    // If ready is removed immediately, the cells in the holes reappear while the
    // fade-out is still going on. Rather noticeable.
    if(Modernizr.csstransforms) {

      boardElement.bind(eventName("transition") + ".board", function() {

        boardElement.unbind(eventName("transition") + ".board");
        boardElement.removeClass("ready");
      });
    } else {
      boardElement.removeClass("ready");
    }
  }
};

/**
 * Disables the board.
 */
Board.prototype.disable = function() {
  $("#board").addClass("disabled");
};

/**
 * Enables the board.
 */
Board.prototype.enable = function() {
  $("#board").removeClass("disabled");
};

/**
 * Map of arrays of dynamic and animation CSS rules to remove when creating a new board.
 * 
 * @private
 */
Board.rules = {
  dynamic: [
    "#board section",
    "#board section.even",
    "#board div",
    "#board div.selected",
    "#board div.adjacent",
    "#board div.jump"
  ],
  animation: [
    "#board div.hole"
  ]
};

/**
 * Function to be used as a callback for managing resizing of the board in response to a
 * change in browser size.
 * 
 * @static
 * @private
 * @param {Number} columns total number of columns in the board
 * @param {Number} largestColumn size of the largest column in the board
 */
Board.resize = function(columns, largestColumn) {

  var rule;
  for(rule in Board.rules.dynamic) {
    removeRule(Board.rules[rule], STYLES["dynamic"]);
  }

  // Size of the board locations is determined by the maximum size which
  // can be given based on the smallest dimension of the parent container.
  var width = $("body").width() - 14;
  var height = $("#board").height();

  var size;
  if(windowSize().width > windowSize().height) {

    // Height is our limiting dimension
    size = height / largestColumn;
  } else {
    // Width is limiting dimension
    size = width / columns;
  }
  var side = size / 2 * Math.cos(Math.PI / 6) * 2 * Math.tan(Math.PI / 6);
  var offset = Math.round((size - side) / 2);
  side = Math.round(side);
  size = Math.round(size);

  // Canvas folks get nice hexagons. Those without get squares (boring!).
  if(Modernizr.canvas) {

    // Generate the hexagon background image.
    var background = document.createElement("canvas");
    background.width = size;
    background.height = size;

    var context = background.getContext("2d");
    context.beginPath();
    context.moveTo(offset, 1);
    context.lineTo(offset + side + 1, 1);
    context.lineTo(size, size / 2);
    context.lineTo(offset + side + 1, size);
    context.lineTo(offset, size);
    context.lineTo(1, size / 2);
    context.lineTo(offset, 1);
    context.closePath();

    var location = paint(context, size, COLORS["location"]);
    var selected = paint(context, size, COLORS["location-selected"]);
    var adjacent = paint(context, size, COLORS["location-adjacent"]);
    var jump = paint(context, size, COLORS["location-jump"]);

    // Set width of columns, even column offsets, and location backgrounds.
    var overlap = Math.ceil(size * .2); // .186411
    insertRule("#board section", "width: " + (size - overlap) + "px", STYLES["dynamic"]);
    insertRule("#board section.even", "padding-top: " + (size / 2) + "px", STYLES["dynamic"]);
    insertRule("#board div", "height: " + size + "px; margin-left: " + (-overlap) + "px", STYLES["dynamic"]);
    insertRule("#board div", "background-image: url(" + location + ")", STYLES["dynamic"]);
    insertRule("#board div.selected", "background-image: url(" + selected + ")", STYLES["dynamic"]);
    insertRule("#board div.adjacent", "background-image: url(" + adjacent + ")", STYLES["dynamic"]);
    insertRule("#board div.jump", "background-image: url(" + jump + ")", STYLES["dynamic"]);
  } else {
    insertRule("#board section", "width: " + size + "px; margin-right: 1px", STYLES["dynamic"]);
    insertRule("#board section.even", "padding-top: " + (size / 2) + "px", STYLES["dynamic"]);
    insertRule("#board div", "height: " + size + "px; margin-bottom: 1px", STYLES["dynamic"]);
    insertRule("#board div", "background-color: " + COLORS["location"], STYLES["dynamic"]);
    insertRule("#board div.selected", "background-color: " + COLORS["location-selected"], STYLES["dynamic"]);
    insertRule("#board div.adjacent", "background-color: " + COLORS["location-adjacent"], STYLES["dynamic"]);
    insertRule("#board div.jump", "background-color: " + COLORS["location-jump"], STYLES["dynamic"]);
  }
};

/**
 * Creates the required DOM elements for the board based on a given layout object. This modifies
 * the page DOM.
 * 
 * @static
 * @private
 * @param {Array} layout nested set of arrays (outer is columns) indicating the board layout
 * @param {Array} players array of player names relative to {@link TOKENS}
 * @param {Function} click function to be called when a location on the board is clicked
 * @returns {Array} index 0 is an array mirroring the input layout array but filled with wrapped DOM objects,
 *                  index 1 is the height of the largest column
 */
Board.createBoard = function(layout, players, click) {

  // Clear out any previous board.
  var boardElement = $("#board");
  boardElement.html("");

  var board = [];
  var largestColumn = 0;

  var column;
  for(column in layout) {
    board[column] = [];

    // Largest column is later used for sizing and centering the board.
    var max = Math.max(largestColumn, layout[column].length);

    if(max !== largestColumn) {
      largestColumn = max;

      if(column % 2 !== 0) {
        largestColumn += .5; // Account for offset from top
      }
    }

    // Create locations from layout array.
    var columnElement = $("<section class='" + (column % 2 ? "even" : "odd") + "'/>");

    var row, obj, initial, insideBoard = false;
    for(row in layout[column]) {

      if(layout[column][row].empty) {
        obj = $("<div class='" + (insideBoard ? "hole" : "empty") + "'/>");
      } else {
        insideBoard = true;

        obj = $("<div/>").click(function(column, row) {
          return function() {
            click(column, row);
          };
        }(Number(column), Number(row)));

        // Add player tokens for initial locations.
        initial = layout[column][row].player;

        if(initial) {
          obj.addClass("player" + initial);
          obj.append($("<img src='" + TOKENS[players[initial]] + "'/>"));
        }
      }
      columnElement.append(obj);

      board[column][row] = obj;
    }

    boardElement.append(columnElement);
  }

  // Adjust the size of the board to account for space taken up by the title and scores.
  boardElement.css("top", ($("header > h1").height() + 7) + "px");
  boardElement.css("bottom", ($("#scores").height() + 7) + "px");

  return [board, largestColumn];
};

/**
 * Sets up the scores area for the players. This modifies the page DOM.
 * 
 * @private
 * @param {Object} model representing the current state of the game
 * @returns {Array} indexed by player number, containing wrapped DOM objects for use indicating score
 */
Board.setupScoring = function(model) {

  // Wipe out old score data and create new indicators.
  var scores = $("#scores");
  scores.html("");

  var indicators = [];
  $.each(model.totals, function(player, total) {

    if(player) {
      var background = "background-color: " + COLORS[model.players[player]];
      var indicator = $("<div style='" + background + "'><span>" + total + "<span></div>");
      indicator.css("width", total * 5);
      indicators[player] = indicator;

      scores.append($("<section/>")
        .append($("<img src='" + TOKENS[model.players[player]] + "'/>"))
        .append(indicator));
    }
  });

  return indicators;
};
