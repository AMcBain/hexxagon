/* Hexxagon [for the web]
 * 
 * Original game by  Argo Games
 *    This clone by  Art McBain
 *
 * Hexxagon 1 and 2 are considered abandonware:
 *   http://www.abandonia.com/en/games/348/Hexxagon.html
 * 
 * 
 * Icons provided by http://p.yusukekamiyamane.com/ from the Fugue set.
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 * 
 * Cursors provided by Marz {Matt} ( http://www.rw-designer.com/cursor-set/blue-insanity-d )
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 *
 * Source code:
 *   https://bitbucket.org/AMcBain/hexxagon
 */

/**
 * @class Inits the various bits and pieces of the main menu.
 * @constructor
 */
function Menu() {

  var player1List = $("#menu section section:first-child ul");
  var player2List = $("#menu section section:first-child+section ul");

  var token, attr, selected1, selected2;
  for(token in TOKENS) {
    attr = "class='" + cssify(token) + "' style='background-image: url(" + TOKENS[token] + ")'";

    player1List.append($("<li " + attr + ">" + token + "</li>").click(function() {
      var $this = $(this);

      // Only select unselected and enabled items.
      if(!$this.hasClass("selected") && !$this.hasClass("disabled")) {

        // Unselect and enable the previous items, then enable this item and disable its pair.
        $("#menu section section:first-child li." + selected1).removeClass("selected");
        $("#menu section section:nth-child(2) li." + selected1).removeClass("disabled");
        $this.addClass("selected");
        // Disabling the pair ensures two players can't have the same token.
        $("#menu section section:nth-child(2) li." + firstClass(this)).addClass("disabled");
        selected1 = firstClass(this);
      }
    }));
    player2List.append($("<li " + attr + ">" + token + "</li>").click(function() {

      var $this = $(this);
      if(!$this.hasClass("selected") && !$this.hasClass("disabled")) {

        $("#menu section section:first-child li." + selected2).removeClass("disabled");
        $("#menu section section:nth-child(2) li." + selected2).removeClass("selected");
        $this.addClass("selected");
        $("#menu section section:first-child li." + firstClass(this)).addClass("disabled");
        selected2 = firstClass(this);
      }
    }));
  }

  selected1 = firstClass($("#menu section section:first-child li:first-child").addClass("selected"));
  $("#menu section section:first-child li:nth-child(2)").addClass("disabled");
  $("#menu section section:nth-child(2) li:first-child").addClass("disabled");
  selected2 = firstClass($("#menu section section:nth-child(2) li:nth-child(2)").addClass("selected"));

  var play = $("#menu section nav ul:first-child");
  play.click(function(scope) {
    return function() {
      scope.setVisible(false);
      if(scope.close instanceof Function) {
        scope.close();
      }
    };
  }(this));
  $("#menu section nav").css("height", $("#menu section").height());

  var menu = $("#menu");
  menu.css("margin-top", (-menu.height() / 2) + "px");
  menu.css("margin-left", (-menu.width() / 2) + "px");

  /**
   * Returns the token selected by the player at the given index.
   * 
   * @param {Number} index of the player to get
   * @returns {string} string indicating the player's choice relative to the {@link TOKENS} map
   */
  this.getPlayer = function(index) {
    return (index == 1 ? selected1 : (index == 2 ? selected2 : null));
  };

  /**
   * Sets the visible status of the menu.
   * 
   * @param {boolean} visible whether the menu should be visible
   */
  this.setVisible = function(visible) {
    if(visible) {
      menu.addClass("active");
    } else {
      menu.removeClass("active");
    }
  };
}
