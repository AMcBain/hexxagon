/* Hexxagon [for the web]
 * 
 * Original game by  Argo Games
 *    This clone by  Art McBain
 *
 * Hexxagon 1 and 2 are considered abandonware:
 *   http://www.abandonia.com/en/games/348/Hexxagon.html
 * 
 * 
 * Icons provided by http://p.yusukekamiyamane.com/ from the Fugue set.
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 * 
 * Cursors provided by Marz {Matt} ( http://www.rw-designer.com/cursor-set/blue-insanity-d )
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 *
 * Source code:
 *   https://bitbucket.org/AMcBain/hexxagon
 */

/**
 * Returns the size of the window.
 * Code paraphrased from <a href="http://www.howtocreate.co.uk/tutorials/javascript/browserwindow">http://www.howtocreate.co.uk/tutorials/javascript/browserwindow</a>.
 * 
 * @returns {Object} of format <code>{width: 0, height: 0}</code>
 */
function windowSize() {
  var size = {};

  if(typeof window.innerWidth === "number") {
    size.width = window.innerWidth;
    size.height = window.innerHeight;
  } else if(document.documentElement) {
    size.width = document.documentElement.clientWidth;
    size.height = document.documentElement.clientHeight;
  }

  return size;
}

/**
 * Inserts a CSS rule based on the given selector, properties,
 * and index of the stylesheet in which to append the new rule.
 * 
 * @param {string} selector to insert
 * @param {string} properties of the selector
 * @param {Number} index of the stylesheet in which to insert this selector
 */
function insertRule(selector, properties, index) {
  var rule, sheet = document.styleSheets[index];
  var rules = sheet.cssRules ? sheet.cssRules : sheet.rules;

  if(sheet.insertRule) {
    sheet.insertRule(selector + " { " + properties + " }", rules ? rules.length : 0);
  } else if(sheet.addRule) {
    properties = properties.split(";");

    for(rule in properties) {
      if(properties[rule]) {
        sheet.addRule(selector, properties[rule] + ";");
      }
    }
  }
}

/**
 * Removes all rules with the given selector in the stylesheet indicated by the given index.
 * 
 * @param {string} selector to find
 * @param {Number} index of the stylesheet to check
 */
function removeRule(selector, index) {
  var i, sheet = document.styleSheets[index];
  var rules = sheet.cssRules ? sheet.cssRules : sheet.rules;

  for (i = 0; rules && i < rules.length; i++){

    if (rules[i].selectorText == selector) {
      if(sheet.deleteRule) {
        sheet.deleteRule(i--);
      } else {
        sheet.removeRule(i--);
      }
    }
  }
}

/**
 * Paints a given Canvas context and returns an image of that Canvas.
 * 
 * @param {CanvasRenderingContext2D} context to be painted
 * @param {Number} size of the context, assuming it is square
 * @param {string} fill "fillStyle" of the context
 * @returns {string} a string containing the context as raw image data
 */
function paint(context, size, fill) {
  context.fillStyle = fill;
  context.fill();
  var data = context.canvas.toDataURL();
  context.clearRect(0, 0, size, size);
  return data;
}

/**
 * Returns the appropriate event name for the given camel-case CSS property name.
 * Borrowed from <a href="http://www.modernizr.com/docs/">Borrowed from http://www.modernizr.com/docs/</a>.
 * 
 * @function
 * @param {string} name of the event
 * @returns {string} name of the event corresponding to the given property
 */
var eventName = (function() {
  var names = {
    'WebkitTransition': 'webkitTransitionEnd',
       'MozTransition': 'transitionend',
         'OTransition': 'oTransitionEnd',
        'msTransition': 'msTransitionEnd',
          'transition': 'transitionend'
  };
  return function(name) {
    return names[Modernizr.prefixed(name)];
  };
}());

/**
 * Returns an appropriately prefixed (-moz-, etc) value for the given CSS selector.
 * 
 * @param {string} value the CSS selector to be prefixed.
 * @returns {string} the value with an appropriate browser prefix
 */
function className(value) {
  var prefix = Modernizr.prefixed(value);

  if(prefix.length !== value.length) {
    prefix = (prefix.charAt(0) === prefix.charAt(0).toLowerCase() ? "-" + prefix : prefix);
    prefix = prefix.replace(/([A-Z])/g, "-$1").toLowerCase();
  }
  return prefix;
}

/**
 * Takes an arbitrary string and returns a value suitable for use as a CSS class name.
 * 
 * @param {string} value to be CSSified
 * @returns {string} a css class name suitable version of the given value
 */
function cssify(value) {
  return value.replace(" ", "_");
}

/**
 * Returns the first CSS class on an object.
 * 
 * @param {Object} object from which to get the class
 * @returns {string} the first CSS class name of the given object
 */
function firstClass(object) {
  return (object.attr ? object.attr("class") : object.className).split(" ")[0];
}

/**
 * Safely indexes into a nested array.
 * 
 * @param {Array} array outer array
 * @param {Number} outer index into the outer array
 * @param {Number} inner index into the inner array
 * @param {string} [property] to retrieve the value of on the object stored at the inner index
 * @returns {Object} the value of the inner array index or undefined if the inner array does not exist
 */
function safeIndex(array, outer, inner, property) {
  if(outer = array[outer]) {
    inner = outer[inner];
    return (property && inner ? inner[property] : inner);
  }
  return outer;
}
