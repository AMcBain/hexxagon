/* Hexxagon [for the web]
 * 
 * Original game by  Argo Games
 *    This clone by  Art McBain
 *
 * Hexxagon 1 and 2 are considered abandonware:
 *   http://www.abandonia.com/en/games/348/Hexxagon.html
 * 
 * 
 * Icons provided by http://p.yusukekamiyamane.com/ from the Fugue set.
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 * 
 * Cursors provided by Marz {Matt} ( http://www.rw-designer.com/cursor-set/blue-insanity-d )
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 *
 * Source code:
 *   https://bitbucket.org/AMcBain/hexxagon
 */

/**
 * @class Dialog for displaying a dialog with a title and content.
 * @constructor
 * @param {string} name used in finding the existing HTMLElement to use
 */
function Dialog(name) {

  var dialog = $("#" + name);
  var dialogTitle = $("#" + name + " header > h1");
  var dialogClose = $("#" + name + " header > img");
  var dialogContent = $("#" + name + " > section");

  dialogClose.click(function(scope) {
    return function() {
      scope.setVisible(false);
      if(scope.close instanceof Function) {
        scope.close();
      }
    };
  }(this));

  /**
   * Sets whether the dialog close button should be visible.
   * 
   * @param {boolean} closeable whether the dialog is closeable
   */
  this.setCloseable = function(closeable) {
    if(closeable) {
      dialog.addClass("closeable");
    } else {
      dialog.removeClass("closeable");
    }
  };

  /**
   * Sets the title of the dialog.
   * 
   * @param {string} title to display
   */
  this.setTitle = function(title) {
    dialogTitle.html(title);
  };

  /**
   * Sets the content of the dialog.
   * 
   * @param {string} content to display
   */
  this.setContent = function(content) {
    dialogContent.html(content);
    dialogContent.css("display", content ? "" : "none");
  };

  /**
   * Sets the visible status of the dialog.
   * 
   * @param {boolean} visible whether the dialog should be visible
   */
  this.setVisible = function(visible) {
    if(visible) {
      dialog.css("marginTop", (-dialog[0].offsetHeight / 2) + "px");
      dialog.css("marginLeft", (-dialog[0].offsetWidth / 2) + "px");
      dialog.addClass("active");
    } else {
      dialog.removeClass("active");
    }
  };
}
