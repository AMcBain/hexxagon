/* Hexxagon [for the web]
 * 
 * Original game by  Argo Games
 *    This clone by  Art McBain
 *
 * Hexxagon 1 and 2 are considered abandonware:
 *   http://www.abandonia.com/en/games/348/Hexxagon.html
 * 
 * 
 * Icons provided by http://p.yusukekamiyamane.com/ from the Fugue set.
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 * 
 * Cursors provided by Marz {Matt} ( http://www.rw-designer.com/cursor-set/blue-insanity-d )
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 *
 * Source code:
 *   https://bitbucket.org/AMcBain/hexxagon
 */

/**
 * @class Manages the model representing the game.
 * @constructor Creates Model instance from the given layout and players
 * @param {Array} layout nested set of arrays (outer is columns) indicating the board layout
 * @param {Array} players array of player names relative to {@link TOKENS}
 */
function Model(layout, players) {

  var data = [];
  var totals = [];

  var column, locations = 0;
  for(column in layout) {
    data[column] = [];

    var row, number;
    for(row in layout[column].replace(/\s+$/,"").split("")) {

      number = Number(layout[column][row]);
      data[column][row] = {
         column: Number(column),
            row: Number(row),
          empty: (number === 0),
         player: (isNaN(number) || number === 0 ? false : number)
      };

      if(data[column][row].player) {
        if(!totals[number]) {
          totals[number] = 0;
        }
        totals[number]++;
      }

      locations++;
    }
  }

  return {
    data: data,
    locked: [],
    totals: totals,
    locations: locations,
    players : players,
    move : {
      player: 1,
      // Selected location (move origin)
      selected: {
        column: null,
        row: null
      },
      // Locations highlighted around the selected location.
      highlighted: []
    }
  };
}

// dummies for documentation purposes only
/**
 * Array of columns, each column is an array.
 * 
 * @type Array
 */
Model.prototype.data = [];

/**
 * Counts of locked locations by player, indexed by player id.
 * 
 * @type Array
 */
Model.prototype.locked = [];

/**
 * Total number of tokens by player, indexed by player id.
 * 
 * @type Array
 */
Model.prototype.totals = [];

/**
 * Total number of occupyable locations.
 * 
 * @type Number
 */
Model.prototype.locations = 0;

/**
 * Array of player names relative to {@link TOKENS}.
 * 
 * @type Array
 */
Model.prototype.players = [];

/**
 * Object describing the current move. It has the following properties:
 * @type Object
 * @example
 * &#8203;{
 *   player, // id of the player who's move it is
 *   selected : { // selected location (move origin)
 *     column,
 *     row
 *   },
 *   highlighted // array of highlighted locations around the move origin
 * }
 */
Model.prototype.move = {};

/**
 * The id of the player who's move it is.
 * 
 * @type Number
 */
Model.prototype.move.player = 0;
