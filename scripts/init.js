/* Hexxagon [for the web]
 * 
 * Original game by  Argo Games
 *    This clone by  Art McBain
 *
 * Hexxagon 1 and 2 are considered abandonware:
 *   http://www.abandonia.com/en/games/348/Hexxagon.html
 * 
 * 
 * Icons provided by http://p.yusukekamiyamane.com/ from the Fugue set.
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 * 
 * Cursors provided by Marz {Matt} ( http://www.rw-designer.com/cursor-set/blue-insanity-d )
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 */

$(document).ready(function() {

  var rules = new Rules();
  var layout = "original";
  var board;

  var menu = new Menu();
  menu.close = function() {
    if (board) {
      board.setPlayer(1);
    }
    board = setupGame(new Model(LAYOUTS[layout], [0, menu.getPlayer(1), menu.getPlayer(2)]));
    board.setVisible(true);
  };

  var howto = new Dialog("howto");
  howto.close = function() {
    board.enable();
  };
  howto.setCloseable(true);

  var dialog = new Dialog("dialog");
  dialog.close = function() {
    board.setVisible(false);
    menu.setVisible(true);
  };
  dialog.setCloseable(true);
  dialog.setContent("");

  $("#options li:first-child").click(function() {
    State.clear();
    board.setVisible(false);
    howto.setVisible(false);
    menu.setVisible(true);
  });
  $("#options li:last-child").click(function() {
    board.disable();
    howto.setVisible(true);
  });

  function setupGame(model) {

    // Information about the player's in progress move.
    var move = model.move;

    var board = new Board(model, function click(column, row) {

      // Check if this is a valid location to select.
      if(!rules.canSelect(model, move, column, row)) {
        return;
      }

      // Handle selection of a player location.
      if(model.data[column][row].player) {
        board.select(move.selected.column, move.selected.row, false);

        // Remove highlight from old adjacent or jump locations.
        var location;
        while(location = move.highlighted.pop()) {
          board.adjacent(move.selected.column + location.column, move.selected.row + location.row, false);
          board.jump(move.selected.column + location.column, move.selected.row + location.row, false);
        }

        // Set new selected location and highlight adjacent or jump locations.
        board.select(move.selected.column = column, move.selected.row = row, true);

        $.each(rules.getAdjacentLocations(model, column, row), function(_, location) {
          var currentColumn = column + location.column;
          var currentRow = row + location.row;

          // Checks which of the total possible adjacent locations can be highlighted.
          if(rules.canHighlight(model, currentColumn, currentRow)) {
            move.highlighted.push(location);
            board.adjacent(currentColumn, currentRow, true);
          }
        });

        $.each(rules.getJumpLocations(model, column, row), function(_, location) {
          var currentColumn = column + location.column;
          var currentRow = row + location.row;

          if(rules.canHighlight(model, currentColumn, currentRow)) {
            move.highlighted.push(location);
            board.jump(currentColumn, currentRow, true);
          }
        });
      } else {
        // Perform move for the player token.
        if(board.isAdjacent(column, row)) {
          board.move(move.player, column, row);
          model.data[column][row].player = move.player;
          model.totals[move.player]++;
        } else {
          board.move(move.player, column, row, move.selected.column, move.selected.row);
          delete model.data[move.selected.column][move.selected.row].player;
          model.data[column][row].player = move.player;
        }

        // Remove highlights
        board.select(move.selected.column, move.selected.row, false);

        var location;
        while(location = move.highlighted.pop()) {
          board.adjacent(move.selected.column + location.column, move.selected.row + location.row, false);
          board.jump(move.selected.column + location.column, move.selected.row + location.row, false);
        }

        // Convert surrounding opponent tokens to the current player's.
        $.each(rules.getAdjacentLocations(model, column, row), function(_, location) {
          var currentColumn = column + location.column;
          var currentRow = row + location.row;

          // If the given index exists and doesn't already have the current player's token on it,
          // replace it and update the model.
          var player = safeIndex(model.data, currentColumn, currentRow, "player");

          if(player && move.player !== player) {
            board.move(move.player, currentColumn, currentRow);
            model.data[currentColumn][currentRow].player = move.player;
            model.totals[player]--;
            model.totals[move.player]++;
          }
        });

        // Update scores
        $.each(model.totals, function(player, total) {
          if(player) {
            board.score(player, total);
          }
        });

        // Change who's turn it is (2+ player aware)
        board.setPlayer(move.player = Math.max(1, (move.player + 1) % model.totals.length));

        // Search the entire board to find all the locked locations. Technically this seems expensive, but
        // it's quite fast for the board sizes used and likely to be used by this program. So no really
        // need for any optimizations. Besides, I think a really large board (many locations) would be too
        // unwieldy to play.
        model.locked = [];

        $.each(model.data, function(_, column) {
          $.each(column, function(_, location) {

            if(rules.isLocationLocked(model, location.column, location.row)) {

              if(!model.locked[location.player]) {
                model.locked[location.player] = 0;
              }
              model.locked[location.player]++;
            }
          });
        });
        State.set(model);

        // isAnyPlayerLocked does will detect a game end via a full board also, negating the need for the
        // isBoardFull check. However if it is desireable to force the game to be played out until the
        // board is full, the isAnyPlayerLocked check could be removed.
        if(rules.isBoardFull(model) || rules.isAnyPlayerLocked(model)) {
          var winner = rules.getHighestScoringPlayer(model);

          if(winner.length === 1) {
            var token = TOKENS[model.players[winner[0]]];
            dialog.setTitle("<p><img src='" + token + "'> Player " + winner[0] + " wins!</p>");
          } else {
            dialog.setTitle("Tie.");
          }
          State.clear();

          board.disable();
          dialog.setVisible(true);
        }

        // Reset move
        delete move.selected.column;
        delete move.selected.row;
      }
    });
    board.setPlayer(move.player);

    return board;
  }

  if(State.exists()) {
    board = setupGame(State.get());
    board.setVisible(true);
  } else {
    menu.setVisible(true);
  }
});
