/* Hexxagon [for the web]
 * 
 * Original game by  Argo Games
 *    This clone by  Art McBain
 *
 * Hexxagon 1 and 2 are considered abandonware:
 *   http://www.abandonia.com/en/games/348/Hexxagon.html
 * 
 * 
 * Icons provided by http://p.yusukekamiyamane.com/ from the Fugue set.
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 * 
 * Cursors provided by Marz {Matt} ( http://www.rw-designer.com/cursor-set/blue-insanity-d )
 *   License is CC-BY ( http://creativecommons.org/licenses/by/3.0/ )
 *
 * Source code:
 *   https://bitbucket.org/AMcBain/hexxagon
 */

/**
 * @class Used to evaluate various conditions of a board relative to a set of rules.
 * @constructor
 */
function Rules() {
}

/**
 * Returns whether the given location can be selected based on the game state, current move
 * and location to be evaluated.
 * 
 * @param {Object} model representing the current game state
 * @param {Object} move object containing information about the current move, of format:<br/>
 * <pre><code>{
 *   player: 1,
 *   // Selected location (move origin)
 *   selected: {
 *     column: null,
 *     row: null
 *   }
 *  }</code></pre>
 * @param {Number} column of the location to be evaluated
 * @param {Number} row of the location to be evaluated
 * @returns {boolean} whether the given location can be selected
 */
Rules.prototype.canSelect = function(model, move, column, row) {

  // Already selected.
  if(move.selected.column === column && move.selected.row === row) {
    return false;
  }

  // Can't select an opponent's token.
  if(model.data[column][row].player && move.player !== model.data[column][row].player) {
    return false;
  }

  // Handle locations with no player tokens.
  if(!model.data[column][row].player) {

    // Can't select an empty location if a valid one hasn't been selected first.
    if(!move.selected.column && !move.selected.row) {
      return false;
    } else {
      var colDiff = Math.abs(move.selected.column - column);
      var rowDiff = Math.abs(move.selected.row - row);

      // Can't select something greater than 2 columns and 1 row or 1 column and 2 rows away
      // from the selected location.
      if((colDiff === 2 && rowDiff === 2) || colDiff > 2 || rowDiff > 2) {
        return false;
      }
    }
  }

  return true;
};

/**
 * Returns an array of locations considered adjacent to the given location.
 * 
 * @param {Object} model representing the current game state
 * @param {Number} column of the location to be evaluated
 * @param {Number} row of the location to be evaluated
 * @returns {Array} of adjacent locations
 */
Rules.prototype.getAdjacentLocations = function(model, column, row) {
  return LOCATION_SELECT[column % 2 ? "odd" : "even"].adjacent;
};

/**
 * Returns an array of locations considered jumpable from the given location.
 * 
 * @param {Object} model representing the current game state
 * @param {Number} column of the location to be evaluated
 * @param {Number} row of the location to be evaluated
 * @returns {Array} of jumpable locations
 */
Rules.prototype.getJumpLocations = function(model, column, row) {
  return LOCATION_SELECT[column % 2 ? "odd" : "even"].jump;
};

/**
 * Returns whether the given column and row can be highlighted relative to the given model.
 * The columns and rows passed in to this function are dependent on the offsets in
 * {@link LOCATION_SELECT} relative to the selected column and row. Thus all column and row
 * locations passed to this function are a superset of the actually highlightable locations.
 * 
 * @param {Object} model representing the current game state
 * @param {Number} column of the location to be evaluated
 * @param {Number} row of the location to be evaluated
 * @returns {boolean} whether the given location can be highlighted
 */
Rules.prototype.canHighlight = function(model, column, row) {
  // Default is to highlight the subset of passed locations that contain no player tokens.
  return !safeIndex(model.data, column, row, "player");
};

/**
 * Returns whether the given location is locked  (the token at the given location cannot make
 * any moves)
 * 
 * @param {Object} model representing the current game state
 * @param {Number} column of the location to be evaluated
 * @param {Number} row of the location to be evaluated
 * @returns {boolean} whether the given location is locked
 */
Rules.prototype.isLocationLocked = function(model, column, row) {

  if(!safeIndex(model.data, column, row, "player")) {
    return false;
  }

  var count = 0;
  $.each(this.getAdjacentLocations(model, column, row), function(_, location) {
    var current = safeIndex(model.data, column + location.column, row + location.row);

    if(current && !current.empty && !current.player) {
      count++;
    }
  });
  $.each(this.getJumpLocations(model, column, row), function(_, location) {
    var current = safeIndex(model.data, column + location.column, row + location.row);

    if(current && !current.empty && !current.player) {
      count++;
    }
  });

  return (count === 0);
};

/**
 * Returns whether any player on the board cannot make any further moves relative to the
 * given model.
 * 
 * @param {Object} model representing the current game state
 * @returns {boolean} whether any player cannot make any further moves
 */
Rules.prototype.isAnyPlayerLocked = function(model) {

  var player, locked;
  for(player = 1; player < model.totals.length && !locked; player++) {

    if(player in model.locked) {
      locked = (model.locked[player] === model.totals[player]);
    } else {
      locked = (model.totals[player] === 0);
    }
  }
  return !!locked;
};

/**
 * Returns whether the board is completely filled with tokens.
 * 
 * @param {Object} model representing the current game state
 * @returns {boolean} whether or not the board is full
 */
Rules.prototype.isBoardFull = function(model) {

  var player, tokens = 0;
  for(player = 1; player < model.totals.length; player++) {
    tokens += model.totals[player];
  }
  return (model.locations === tokens);
};

/**
 * Returns an array of the highest scoring player or multiple players if there is a tie for
 * the highest score.
 * 
 * @param {Object} model representing the current game state
 * @returns {Array} an array
 */
Rules.prototype.getHighestScoringPlayer = function(model) {

  var player, max = [1];
  for(player = 2; player < model.totals.length; player++) {

    if(model.totals[player] > model.totals[max[0]]) {
      max = [player];
    } else if(model.totals[player] === model.totals[max[0]]) {
      max.push(player);
    }
  }
  return max;
};
